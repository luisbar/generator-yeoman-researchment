[Yeoman](https://yeoman.io/) helps you to kickstart new projects, prescribing best practices and tools to help you stay productive.

To do so, we provide a generator ecosystem. A generator is basically a plugin that can be run with the `yo` command to scaffold complete projects or useful parts.

## Tips and tricks
- The generator must have the following:
  - The folder must be named generator-name
  - On the `package.json` file:
    - The `name` property must be prefixed by generator-
    - The `keywords` property must contain "yeoman-generator"
    - The `files` property must be an array of files and directories that is used by your generator
    - You should make sure you set the latest version of `yeoman-generator`
- We are using `yeoman-generator` 5.9.0 because we have to use `yeoman-environment` 3.18.3 due to an error on last version of `yeoman-environment`
  - I faced two errors on newer versions
    - sharedFs.pipeline is not a function
    - the requested module 'mem-fs' does not provide an export named 'loadFile'