module.exports = {
	testTimeout: 10000,
	coverageDirectory: "coverage",
	testEnvironment: "node",
	rootDir: ".",
	roots: ["."],
  testMatch: [
    "**/__tests__/**/*.[jt]s?(x)",
    "**/?(*.)+(spec|test).[tj]s?(x)",
    "**/__tests__/**/*.mjs",
    "**/?(*.)+(spec|test).mjs",
  ],
};
