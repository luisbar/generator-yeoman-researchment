import Generator from "yeoman-generator";

export default class extends Generator {

  constructor(args, opts) {
    super(args, opts);
    this.env.options.nodePackageManager = "yarn";
  }

  async prompting() {
    this.answers = await this.prompt([
      {
        type: "input",
        name: "appName",
        message: "Your project name",
        default: this.appname,
      },
      {
        type: "input",
        name: "description",
        message: "Your project description",
      },
      {
        type: "confirm",
        name: "isWebHookEnabled",
        message: "Would you like to enable the web hook?",
      },
      {
        type: "list",
        name: "webHookAuth",
        message: "Select one of the web hook authentication methods",
        when: (answers) => answers.isWebHookEnabled,
        choices: [
          {
            value: "signature",
            name: "Signature",
          },
          {
            value: "basic",
            name: "Basic",
          },
        ],
      },
      {
        type: "checkbox",
        name: "webHookActionsForDeposit",
        message: "Select one or more deposit web hook actions",
        when: (answers) => answers.isWebHookEnabled,
        choices: [
          {
            value: "exchangeDepositAmount",
            name: "Exchange deposit amount",
          },
          {
            value: "sendFeeTransaction",
            name: "Send fee transaction",
          },
          {
            value: "sendDepositTransaction",
            name: "Send deposit transaction",
          },
          {
            value: "issueVirtualCurrency",
            name: "Issue virtual currency",
          },
          {
            value: "blockAccount",
            name: "Bock account",
          },
          {
            value: "sendNotificationToDestinationForDeposit",
            name: "Send notification to destination for deposit",
          },
        ]
      },
      {
        type: "checkbox",
        name: "webHookActionsForWithdrawal",
        message: "Select one or more withdrawal web hook actions",
        when: (answers) => answers.isWebHookEnabled,
        choices: [
          {
            value: "destroyVirtualCurrency",
            name: "Destroy virtual currency",
          },
          {
            value: "sendNotificationToSourceForWithdrawal",
            name: "Send notification to source for withdrawal",
          },
        ],
      },
      {
        type: "confirm",
        name: "isWebSocketEnabled",
        message: "Would you like to enable the web socket?",
      },
      {
        type: "checkbox",
        name: "webSocketEvents",
        message: "Select one or more web socket events",
        when: (answers) => answers.isWebSocketEnabled,
        choices: [
          {
            value: "identityCreated",
            name: "Identity created",
          },
          {
            value: "transactionDone",
            name: "Transaction done",
          },
          {
            value: "transactionFailed",
            name: "Transaction failed",
          },
        ],
      },
      {
        type: "checkbox",
        name: "webSocketActionsForIdentityCreated",
        message: "Select one or more web socket actions for identity created",
        when: (answers) => answers.webSocketEvents.includes("identityCreated"),
        choices: [
          {
            value: "createVirtualAccount",
            name: "Create virtual account",
          },
        ],
      },
      {
        type: "checkbox",
        name: "webSocketActionsForTransactionDoneOfSendType",
        message: "Select one or more web socket actions for transaction done of send type",
        when: (answers) => answers.webSocketEvents.includes("transactionDone"),
        choices: [
          {
            value: "sendNotificationToSourceForTransactionDoneOfSendType",
            name: "Send notification to source for transaction done of send type",
          },
          {
            value: "sendNotificationToDestinationForTransactionDoneOfSendType",
            name: "Send notification to destination for transaction done of send type",
          }
        ],
      },
      {
        type: "checkbox",
        name: "webSocketActionsForTransactionDoneOfPaymentType",
        message: "Select one or more web socket actions for transaction done of payment type",
        when: (answers) => answers.webSocketEvents.includes("transactionDone"),
        choices: [
          {
            value: "sendNotificationToSourceForTransactionDoneOfPaymentType",
            name: "Send notification to source for transaction done of payment type",
          },
          {
            value: "sendNotificationToDestinationForTransactionDoneOfPaymentType",
            name: "Send notification to destination for transaction done of payment type",
          }
        ],
      },
      {
        type: "checkbox",
        name: "webSocketActionsForTransactionDoneOfWithdrawalType",
        message: "Select one or more web socket actions for transaction done of withdrawal type",
        when: (answers) => answers.webSocketEvents.includes("transactionDone"),
        choices: [
          {
            value: "withdrawFiat",
            name: "Withdraw fiat",
          },
        ],
      },
      {
        type: "list",
        name: "paymentGateway",
        message: "Select one of the payment gateways",
        when: (answers) => answers.webSocketActionsForTransactionDoneOfWithdrawalType.includes("withdrawFiat"),
        choices: [
          {
            value: "mono",
            name: "Mono",
          },
          {
            value: "paymentsWay",
            name: "Payments Way",
          },
          {
            value: "payOnTime",
            name: "Pague a tiempo",
          },
        ],
      },
      {
        type: "checkbox",
        name: "webSocketActionsForTransactionFailed",
        message: "Select one or more web socket actions for transaction failed",
        when: (answers) => answers.webSocketEvents.includes("transactionFailed"),
        choices: [
          {
            value: "sendNotificationToSourceForTransactionFailed",
            name: "Send notification to source for transaction failed",
          }
        ],
      },
      {
        type: "confirm",
        name: "isCronJobEnabled",
        message: "Would you like to enable the cron job?",
      },
      {
        type: "checkbox",
        name: "cronJobActions",
        message: "Select one or more cron job actions",
        when: (answers) => answers.isCronJobEnabled,
        choices: [
          {
            value: "unblockAccount",
            name: "Unblock account",
          },
        ],
      },
      {
        type: "confirm",
        name: "isDatabaseEnabled",
        message: "Would you like to enable a Sqlite database?",
      },
      {
        type: "checkbox",
        name: "databaseTables",
        message: "Select one or more database tables",
        when: (answers) => answers.isDatabaseEnabled,
        choices: [
          {
            value: "accounts",
            name: "Accounts",
          },
        ],
      },
    ]);

    this.answers.webHookUsesSignature = this.answers.webHookAuth === "signature";
    this.answers.webHookUsesBasicAuth = this.answers.webHookAuth === "basic";

    this.answers.isExchangeRateEnabled = this.answers.webHookActionsForDeposit.includes("exchangeDepositAmount");
    this.answers.isSendFeeTransactionEnabled = this.answers.webHookActionsForDeposit.includes("sendFeeTransaction");
    this.answers.isSendDepositTransactionEnabled = this.answers.webHookActionsForDeposit.includes("sendDepositTransaction");
    this.answers.isIssueVirtualCurrencyEnabled = this.answers.webHookActionsForDeposit.includes("issueVirtualCurrency");
    this.answers.isBlockAccountEnabled = this.answers.webHookActionsForDeposit.includes("blockAccount");
    this.answers.isSendNotificationToDestinationForDepositEnabled = this.answers.webHookActionsForDeposit.includes("sendNotificationToDestinationForDeposit");

    this.answers.isDestroyVirtualCurrencyEnabled = this.answers.webHookActionsForWithdrawal.includes("destroyVirtualCurrency");
    this.answers.isSendNotificationToSourceForWithdrawalEnabled = this.answers.webHookActionsForWithdrawal.includes("sendNotificationToSourceForWithdrawal");

    this.answers.isIdentityCreatedEventEnabled = this.answers.webSocketEvents.includes("identityCreated");
    this.answers.isCreateVirtualAccountEnabled = this.answers.webSocketActionsForIdentityCreated.includes("createVirtualAccount");

    this.answers.isTransactionDoneEventEnabled = this.answers.webSocketEvents.includes("transactionDone");
    this.answers.isSendNotificationToSourceForTransactionDoneOfSendTypeEnabled = this.answers.webSocketActionsForTransactionDoneOfSendType.includes("sendNotificationToSourceForTransactionDoneOfSendType");
    this.answers.isSendNotificationToDestinationForTransactionDoneOfSendTypeEnabled = this.answers.webSocketActionsForTransactionDoneOfSendType.includes("sendNotificationToDestinationForTransactionDoneOfSendType");
    this.answers.isSendNotificationToSourceForTransactionDoneOfPaymentTypeEnabled = this.answers.webSocketActionsForTransactionDoneOfPaymentType.includes("sendNotificationToSourceForTransactionDoneOfPaymentType");
    this.answers.isSendNotificationToDestinationForTransactionDoneOfPaymentTypeEnabled = this.answers.webSocketActionsForTransactionDoneOfPaymentType.includes("sendNotificationToDestinationForTransactionDoneOfPaymentType");
    this.answers.isWithdrawFiatEnabled = this.answers.webSocketActionsForTransactionDoneOfWithdrawalType.includes("withdrawFiat");
    this.answers.isPaymentsWayEnabled = this.answers.paymentGateway === "paymentsWay";
    this.answers.isMonoEnabled = this.answers.paymentGateway === "mono";
    this.answers.isPayOnTimeEnabled = this.answers.paymentGateway === "payOnTime";

    this.answers.isTransactionFailedEventEnabled = this.answers.webSocketEvents.includes("transactionFailed");
    this.answers.isSendNotificationToSourceForTransactionFailedEnabled = this.answers.webSocketActionsForTransactionFailed.includes("sendNotificationToSourceForTransactionFailed");

    this.answers.isUnblockAccountEnabled = this.answers.cronJobActions.includes("unblockAccount");

    this.answers.isAccountsTableEnabled = this.answers.databaseTables.includes("accounts");
  }

  _updatePackageJson() {
    const packageJson = {
      name: this.answers.appName,
      description: this.answers.description,
      version: "0.0.0",
      main: "src/index.js",
      keywords: [
        "orangepill",
        "agent",
        "wallet"
      ],
      devDependencies: {
        "eslint": "8.53.0",
        "eslint-config-prettier": "9.0.0",
        "eslint-plugin-prettier": "5.0.1",
        "prettier": "3.1.0"
      },
      dependencies: {
        "axios": "1.6.2",
        "dotenv": "16.3.1",
        "envalid": "8.0.0",
        "express": "4.18.2",
        "pino": "8.16.2",
        "kraken-js": "2.5.0"
      },
      volta: {
        "node": "20.13.1",
        "yarn": "1.22.22"
      },
      scripts: {
        "start": "node src/index.js",
        "start:dev": "node --watch src/index.js",
        "start:prod": "pm2 start src/index.js --name " + this.answers.appName
      }
    };

    if (this.answers.isWebSocketEnabled)
      packageJson.dependencies["ws"] = "8.14.2";
    if (this.answers.isCronJobEnabled)
      packageJson.dependencies["node-cron"] = "3.0.3";
    if (this.answers.isDatabaseEnabled)
      packageJson.dependencies = {
        ...packageJson.dependencies,
        "knex": "3.1.0",
        "sqlite3": "5.1.7"
      };

    this.fs.extendJSON(this.destinationPath("package.json"), packageJson);
  }

  _createUnconditionalFiles() {
    // this.fs.copyTpl(
    //   this.templatePath("README.md"),
    //   this.destinationPath("README.md"),
    //   {
    //     ...this.answers,
    //   },
    // );
    // this.fs.copyTpl(
    //   this.templatePath(".prettierrc.js"),
    //   this.destinationPath(".prettierrc.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath(".prettierignore"),
    //   this.destinationPath(".prettierignore"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath(".gitignore"),
    //   this.destinationPath(".gitignore"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath(".eslintrc.js"),
    //   this.destinationPath(".eslintrc.js"),
    // );
    this.fs.copyTpl(
      this.templatePath(".env-example.ejs"),
      this.destinationPath(".env-example"),
      {
        ...this.answers,
      },
    );
    // this.fs.copyTpl(
    //   this.templatePath("src/web-server.ejs"),
    //   this.destinationPath("src/web-server.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/index.ejs"),
    //   this.destinationPath("src/index.js"),
    //   {
    //     ...this.answers,
    //   },
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/utils/signature-verifier.ejs"),
    //   this.destinationPath("src/utils/signature-verifier.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/utils/logger.ejs"),
    //   this.destinationPath("src/utils/logger.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/errors/orangepill-errors.ejs"),
    //   this.destinationPath("src/errors/orangepill-errors.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/utils/config.ejs"),
    //   this.destinationPath("src/utils/config.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/services/orangepill-api.ejs"),
    //   this.destinationPath("src/services/orangepill-api.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/routes/payment-notification.ejs"),
    //   this.destinationPath("src/routes/payment-notification.js"),
    //   {
    //     ...this.answers,
    //   },
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/routes/index.ejs"),
    //   this.destinationPath("src/routes/index.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/middlewares/web-hook-auth.ejs"),
    //   this.destinationPath("src/middlewares/web-hook-auth.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/config/index.ejs"),
    //   this.destinationPath("src/config/index.js"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/config/stage.jseon"),
    //   this.destinationPath("src/config/stage.json"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/config/production.jseon"),
    //   this.destinationPath("src/config/production.json"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/config/development.jseon"),
    //   this.destinationPath("src/config/development.json"),
    // );
    // this.fs.copyTpl(
    //   this.templatePath("src/config/config.jseon"),
    //   this.destinationPath("src/config/config.json"),
    // );
  }

  _createConditionalFiles() {
    if (this.answers.isWebSocketEnabled) {
      this.fs.copyTpl(
        this.templatePath("src/web-socket/socket-client.ejs"),
        this.destinationPath("src/web-socket/socket-client.js"),
      );
      this.fs.copyTpl(
        this.templatePath("src/web-socket/process-event.ejs"),
        this.destinationPath("src/web-socket/process-event.js"),
      );
    }
    if (this.answers.isCronJobEnabled) {
      this.fs.copyTpl(
        this.templatePath("src/jobs/unblock-account.ejs"),
        this.destinationPath("src/jobs/unblock-account.js"),
      );
    }
    if (this.answers.isDatabaseEnabled) {
      this.fs.copyTpl(
        this.templatePath("src/db/knex.ejs"),
        this.destinationPath("src/db/knex.js"),
      );
      this.fs.copyTpl(
        this.templatePath("src/db/knex-config.ejs"),
        this.destinationPath("src/db/knex-config.js"),
      );
      this.fs.copyTpl(
        this.templatePath("src/db/models/Accounts.ejs"),
        this.destinationPath("src/db/models/Accounts.js"),
      );
    }
  }

  writing() {
    this._updatePackageJson();
    this._createUnconditionalFiles();
    // this._createConditionalFiles();
  }
};