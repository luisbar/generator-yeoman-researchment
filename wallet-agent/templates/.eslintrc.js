module.exports = {
	root: true,
	env: {
		node: true,
		commonjs: true,
		es6: true,
		jquery: false,
		jest: true,
		jasmine: true,
	},
	extends: ["eslint:recommended", "plugin:prettier/recommended"],
	parserOptions: {
		sourceType: "module",
		ecmaVersion: "2020",
	},
	rules: {
		"no-console": "warn",
		"no-var": ["error"],
		eqeqeq: ["warn", "always"],
		"prefer-const": [
			"error",
			{
				destructuring: "any",
				ignoreReadBeforeAssign: false,
			},
		],
		"no-unsafe-optional-chaining": [
			"warn",
			{
				disallowArithmeticOperators: true,
			},
		],
		"no-unused-vars": ["warn"],
		"no-mixed-spaces-and-tabs": ["warn"],
		"prettier/prettier": "error",
	},
};
