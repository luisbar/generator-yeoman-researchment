This project can be used as Wallet Agent for Orangepill customer

## How to run it?
- Install [Volta](https://volta.sh/)
- Install dependencies
- Run `yarn start:dev`

## Project structure
- Config folder
  - It contains a json file per environment and it is loaded by [Krakenjs](https://krakenjs.com/)
    - Here you can put config data such as service urls, supported currencies, etc
  - In the `config.json` file you can add middlewares to a single route, multiple routes or all routes by using `middleware` prop 
    - You can add middlewares from node_modules or custom middlewares
    - They can have priorities
    - You can turn on or turn off
    - You can pass arguments
  - In addition you can find the `secrets.js` file which validates env vars
<%_ if (isDatabaseEnabled) { _%>
- Db
  - It contains database models for [Knex](https://knexjs.org/)
  - In addition it has Knex config file
<%_ } _%>
- Errors
  - It has error objects per each consumed service
<%_ if (isCronJobEnabled) { _%>
- Jobs
  - It contains jobs created with [Node Cron](https://github.com/node-cron/node-cron)
<%_ } _%>
- Middlewares
  - Here you can put all your custom middlewares to be loaded by Krakenjs
<%_ if (isWebHookEnabled) { _%>
- Routes
  - Here you can put all your Express routes
<%_ } _%>
- Services
  - Here you can put all invocations to other APIs
- Utils
  - Util scripts
<%_ if (isWebSocketEnabled) { _%>
  - Web socket
    - It has handlers for events
<%_ } _%>

## Flags per modules, events and actions
- modules
<%_ if (isWebHookEnabled) { _%>
  - web hook (isWebHookEnabled)
    - deposit
      - auth by signature (webHookUsesSignature) (deposit)
      - auth by basic auth (webHookUsesBasicAuth) (deposit)
      - exchange rate (isExchangeRateEnabled) (deposit)
      - send fee transaction (isSendFeeTransactionEnabled) (deposit)
      - send deposit transaction (isSendDepositTransactionEnabled) (deposit)
      - issue virtual currency (isIssueVirtualCurrencyEnabled) (deposit)
      - block account (isBlockAccountEnabled) (deposit)
      - send notification to destination (isSendNotificationToDestinationForDepositEnabled) (deposit)
    - withdrawal
      - auth by signature (webHookUsesSignature) (deposit)
      - auth by basic auth (webHookUsesBasicAuth) (deposit)
      - destroy virtual currency (isDestroyVirtualCurrencyEnabled) (withdrawal)
      - send notification to source (isSendNotificationToSourceForWithdrawalEnabled) (withdrawal)
<%_ } _%>
<%_ if (isWebSocketEnabled) { _%>
  - web socket (isWebSocketEnabled)
    - web socket events
      - identity created (isIdentityCreatedEventEnabled)
        - create virtual account (isCreateVirtualAccountEnabled)
      - transaction done (isTransactionDoneEventEnabled)
        - send transaction
          - send notification to source (isSendNotificationToSourceForTransactionDoneOfSendTypeEnabled)
          - send notification to destination (isSendNotificationToDestinationForTransactionDoneOfSendTypeEnabled)
        - payment transaction
          - send notification to source (isSendNotificationToSourceForTransactionDoneOfPaymentTypeEnabled)
          - send notification to destination (isSendNotificationToDestinationForTransactionDoneOfPaymentTypeEnabled)
        - withdrawal transaction
          - withdrawal (isWithdrawFiatEnabled)
            - payment gateways (we can use it to map body on middleware)
              - payments way (isPaymentsWayEnabled)
              - mono (isMonoEnabled)
              - pay on time (isPayOnTimeEnabled)
      - transaction failed event (isTransactionFailedEventEnabled)
        - all transaction types
          - send notification to source (isSendNotificationToSourceForTransactionFailedEnabled)
<%_ } _%>
<%_ if (isCronJobEnabled) { _%>
  - cron job (isCronJobEnabled)
      - unblock account (isUnblockAccountEnabled)
<%_ } _%>
<%_ if (isDatabaseEnabled) { _%>
  - database (isDatabaseEnabled)
      - accounts table (isAccountsTableEnabled)
<%_ } _%>