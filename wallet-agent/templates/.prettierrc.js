module.exports = {
	tabWidth: 1,
	useTabs: true,
	singleQuote: false,
	semi: true,
	trailingComma: "es5",
	bracketSpacing: true,
};
