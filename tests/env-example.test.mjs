import helpers from "yeoman-test";
import assert from "yeoman-assert";
import path from "path";

const WALLET_AGENT_PATH = `${path.resolve()}/wallet-agent`;
let defaultAnswers = {
  appName: "test",
  description: "desc",
  isWebHookEnabled: true,
  webHookAuth: "signature",
  webHookActionsForDeposit: [
    "exchangeDepositAmount",
    "sendFeeTransaction",
    "sendDepositTransaction",
    "issueVirtualCurrency",
    "blockAccount",
    "sendNotificationToDestinationForDeposit"
  ],
  webHookActionsForWithdrawal: [
    "destroyVirtualCurrency",
    "sendNotificationToSourceForWithdrawal"
  ],
  isWebSocketEnabled: true,
  webSocketEvents: [
    "identityCreated",
    "transactionDone",
    "transactionFailed"
  ],
  webSocketActionsForIdentityCreated: [
    "createVirtualAccount"
  ],
  webSocketActionsForTransactionDoneOfSendType: [
    "sendNotificationToSourceForTransactionDoneOfSendType",
    "sendNotificationToDestinationForTransactionDoneOfSendType"
  ],
  webSocketActionsForTransactionDoneOfPaymentType: [
    "sendNotificationToSourceForTransactionDoneOfPaymentType",
    "sendNotificationToDestinationForTransactionDoneOfPaymentType"
  ],
  webSocketActionsForTransactionDoneOfWithdrawalType: [
    "withdrawFiat"
  ],
  paymentGateway: "mono",
  webSocketActionsForTransactionFailed: [
    "sendNotificationToSourceForTransactionFailed"
  ],
  isCronJobEnabled: true,
  cronJobActions: [
    "unblockAccount"
  ],
  isDatabaseEnabled: true,
  databaseTables: [
    "accounts"
  ]
};

describe(".env-example", () => {
  it("should create file with default values", () => {
    return helpers.run(WALLET_AGENT_PATH)
    .withAnswers({
      ...defaultAnswers,
      paymentGateway: "nonSupportedPaymentGateway",
      webHookAuth: "nonSupportedWebHookAuth",
    })
    .then(function() {
      assert.file([".env-example"]);
      assert.fileContent(
        ".env-example",
        `NODE_ENV=development
ORANGEPILL_API_KEY=`,
      );
    });
  });
  
  it.each([
    [
      {
        paymentGateway: "mono",
        webHookAuth: "nonSupportedWebHookAuth",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=
MONO_API_KEY=`,
    ],
    [
      {
        paymentGateway: "paymentsWay",
        webHookAuth: "nonSupportedWebHookAuth",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=
PAYMENTS_WAY_USERNAME=
PAYMENTS_WAY_PASSWORD=`,
    ],
  ])("should create file with credentials for paymentGateway: $paymentGateway", ({ paymentGateway, ...rest }, expectedFileContent) => {
    return helpers.run(WALLET_AGENT_PATH)
    .withAnswers({
      ...defaultAnswers,
      ...rest,
      paymentGateway,
    })
    .then(function() {
      assert.file([".env-example"]);
      assert.fileContent(
        ".env-example",
        expectedFileContent,
      );
    });
  });

  it.each([
    [
      {
        paymentGateway: "nonSupportedPaymentGateway",
        webHookAuth: "signature",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=`,
    ],
    [
      {
        paymentGateway: "nonSupportedPaymentGateway",
        webHookAuth: "basic",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=`,
    ],
  ])("should create file with credentials for webHookAuth: $webHookAuth", ({ webHookAuth, ...rest }, expectedFileContent) => {
    return helpers.run(WALLET_AGENT_PATH)
    .withAnswers({
      ...defaultAnswers,
      ...rest,
      webHookAuth,
    })
    .then(function() {
      assert.file([".env-example"]);
      assert.fileContent(
        ".env-example",
        expectedFileContent,
      );
    });
  });

  it.each([
    [
      {
        paymentGateway: "mono",
        webHookAuth: "signature",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=
MONO_API_KEY=
MONO_WEBHOOK_SIGNATURE_SECRET=`,
    ],
    [
      {
        paymentGateway: "paymentsWay",
        webHookAuth: "basic",
      },
      `NODE_ENV=development
ORANGEPILL_API_KEY=
PAYMENTS_WAY_USERNAME=
PAYMENTS_WAY_PASSWORD=
PAYMENTS_WAY_WEBHOOK_COMMERCE_NAME=
PAYMENTS_WAY_WEBHOOK_COMMERCE_API_KEY=`,
    ],
  ])("should create file with credentials for webHookAuth: $webHookAuth", ({ webHookAuth, ...rest }, expectedFileContent) => {
    return helpers.run(WALLET_AGENT_PATH)
    .withAnswers({
      ...defaultAnswers,
      ...rest,
      webHookAuth,
    })
    .then(function() {
      assert.file([".env-example"]);
      assert.fileContent(
        ".env-example",
        expectedFileContent,
      );
    });
  });
});